﻿using Covid19.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.DAL.CRUD
{
    public class StateCRUD
    {
        public int GetStateId(string state)
        {
            using var ctx = new coviddbContext();
            return ctx.States.Where(s => s.Name.ToLower().Contains(state.ToLower())).Select(s => s.Id).FirstOrDefault();
        }
    }
}
