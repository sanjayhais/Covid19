﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Covid19.DAL.Models;
namespace Covid19.DAL.CRUD
{
    public class UserCRUD
    {
        public User Register(User user)
        {
            using var ctx = new coviddbContext();
            
                var newUser = ctx.Users.Add(user);
                ctx.SaveChanges();
                return newUser.Entity;
            
        }
        public DAL.Models.User GetFirst(System.Linq.Expressions.Expression<Func<Models.User, bool>> query)
        {
           
                using var ctx = new coviddbContext();
                return ctx.Users.FirstOrDefault(query);
           

        }
    }
}
