﻿using Covid19.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.DAL.CRUD
{
    public class TotalCRUD
    {
        public Totaldatum getStateData(int state)
        {
            
            using var ctx = new coviddbContext();
            return ctx.Totaldata
                .Include(p=>p.State)
                .FirstOrDefault(t => t.Stateid == state);
            
        }
    }
}
