﻿using Covid19.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Covid19.DAL.CRUD
{
    public class TokenCRUD
    {
        public (Guid,User) NewToken (User user)
        {
            using var ctx = new coviddbContext();
            var newToken = ctx.Tokens.Add(new Token()
            {
                 Token1= Guid.NewGuid(),
                Userid = user.Id,
            });
            ctx.SaveChanges();
            var resp = (newToken == null) ? Guid.Empty : newToken.Entity.Token1;

            var obj = (newToken != null) ? newToken.Entity.User : null;
            return (resp, obj);
        }

        public (Guid,User) Verify(Guid guid)
        {
            using var ctx = new coviddbContext();
            var token = ctx.Tokens.Include(u=>u.User).FirstOrDefault(t=>t.Token1 == guid);
            var resp = (token==null)?Guid.Empty:token.Token1;

            var obj = (token != null) ? token.User : null;
            return (resp , obj);
        }
    }
}
