﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class State
    {
        public State()
        {
            Dailydata = new HashSet<Dailydatum>();
            Totaldata = new HashSet<Totaldatum>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Dailydatum> Dailydata { get; set; }
        public virtual ICollection<Totaldatum> Totaldata { get; set; }
    }
}
