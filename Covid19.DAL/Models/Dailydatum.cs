﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class Dailydatum
    {
        public int Id { get; set; }
        public DateTime Datetime { get; set; }
        public int Stateid { get; set; }
        public int? Newcases { get; set; }
        public int? Recovered { get; set; }
        public int? Deaths { get; set; }
        public int? Active { get; set; }

        public virtual State State { get; set; }
    }
}
