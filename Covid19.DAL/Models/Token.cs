﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class Token
    {
        public int Id { get; set; }
        public Guid Token1 { get; set; }
        public int Userid { get; set; }

        public virtual User User { get; set; }
    }
}
