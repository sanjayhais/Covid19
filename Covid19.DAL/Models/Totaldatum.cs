﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class Totaldatum
    {
        public int Id { get; set; }
        public int Stateid { get; set; }
        public long? Totalcases { get; set; }
        public long? Totaldeaths { get; set; }
        public long? Totalrecovered { get; set; }
        public long? Totalactive { get; set; }

        public virtual State State { get; set; }
    }
}
