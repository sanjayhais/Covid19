﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class coviddbContext : DbContext
    {
        public coviddbContext()
        {
            
        }

        public coviddbContext(DbContextOptions<coviddbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Dailydatum> Dailydata { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<Totaldatum> Totaldata { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                // optionsBuilder.UseSqlServer("Data Source=db;Database=coviddb;Integrated Security=false;User ID=SA;Password=Monday*1;Connection Timeout=60;");
                optionsBuilder.UseSqlServer("Data Source =.; Database = coviddb; Integrated Security = True; Persist Security Info = False; Pooling = False; MultipleActiveResultSets = False; Connect Timeout = 60; Encrypt = False; TrustServerCertificate = False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Dailydatum>(entity =>
            {
                entity.ToTable("dailydata");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Datetime)
                    .HasPrecision(0)
                    .HasColumnName("datetime");

                entity.Property(e => e.Deaths).HasColumnName("deaths");

                entity.Property(e => e.Newcases).HasColumnName("newcases");

                entity.Property(e => e.Recovered).HasColumnName("recovered");

                entity.Property(e => e.Stateid).HasColumnName("stateid");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Dailydata)
                    .HasForeignKey(d => d.Stateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dailydata_states");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("states");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Token>(entity =>
            {
                entity.ToTable("token");

                entity.Property(e => e.Token1).HasColumnName("token");

                entity.Property(e => e.Userid).HasColumnName("userid");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Tokens)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_token_user");
            });

            modelBuilder.Entity<Totaldatum>(entity =>
            {
                entity.ToTable("totaldata");

                entity.Property(e => e.Stateid).HasColumnName("stateid");

                entity.Property(e => e.Totalactive).HasColumnName("totalactive");

                entity.Property(e => e.Totalcases).HasColumnName("totalcases");

                entity.Property(e => e.Totaldeaths).HasColumnName("totaldeaths");

                entity.Property(e => e.Totalrecovered).HasColumnName("totalrecovered");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Totaldata)
                    .HasForeignKey(d => d.Stateid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_totaldata_states");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("username");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
