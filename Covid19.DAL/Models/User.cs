﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Covid19.DAL.Models
{
    public partial class User
    {
        public User()
        {
            Tokens = new HashSet<Token>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Token> Tokens { get; set; }
    }
}
