﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using RequestResponseModels.Request;
using RequestResponseModels.Response;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Covid19.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class User : ControllerBase
    {
        private UserServices _userService;
        private TokenServices _tokenService;
        public User(UserServices userService, TokenServices tokenService)
        {
            this._userService = userService;
            this._tokenService = tokenService;
        }

        [HttpPost]

        public ActionResult<TokenResponse> Register([FromBody] RegisterRequest req)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception(ModelState.Values.SelectMany(v => v.Errors).ToString());
            }

            var user = _userService.Register(req.Email, req.Username, req.Password);
            _ = _tokenService.GetNewLoginToken(user);
            return new TokenResponse()
            {
                Token = _tokenService.AccessToken
            };

        }

        [HttpPost]

        public ActionResult<TokenResponse> Login([FromForm] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception(ModelState.Values.SelectMany(v => v.Errors).ToString());
            }
            // verify user
            var currentUser = _userService.VerifyUser(request.Username, request.Password);

            //return token
            _ = currentUser ?? throw new Exception("User Not Found with given credentials");
            _ = _tokenService.GetNewLoginToken(currentUser);
            return new TokenResponse()
            {
                Token = _tokenService.AccessToken
            };
        }
    }
}
