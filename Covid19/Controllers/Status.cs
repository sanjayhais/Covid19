﻿using Microsoft.AspNetCore.Mvc;
using RequestResponseModels.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Covid19.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class Status : ControllerBase
    {

        [HttpGet]
        public ActionResult<StatusResponse> Get()
        {
            return new StatusResponse()
            {
                DatabaseName = "localhost/CovidDb",
                Status = "UP",
                Version = "1.0.3"
            };
        }

    }
}
