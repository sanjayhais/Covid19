﻿using Covid19.Middllewares;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RequestResponseModels.Request;
using RequestResponseModels.Response;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Covid19.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class Covid : ControllerBase
    {
        private readonly TokenServices _tokenService;
        private readonly CovidServices _covidServices;
        public Covid(TokenServices tokenServices, CovidServices covidServices) 
        {
            this._tokenService = tokenServices;
            this._covidServices = covidServices;
        }
        [HttpPost]
        public ActionResult<StateDataResponse> GetStateData([FromBody] StateDataRequest request)
        {
            var state = (request.State == null) ? "TOTAL" : request.State;
            var data = _covidServices.StateData(state);
            return Unauthorized();
            return new StateDataResponse()
            {
                StateName = data.State.Name,
                Active = data.Totalactive.Value,
                Decesed = data.Totaldeaths.Value,
                Recovered = data.Totalrecovered.Value,
                TotalCases = data.Totalcases.Value

            };
        }
    }
}
