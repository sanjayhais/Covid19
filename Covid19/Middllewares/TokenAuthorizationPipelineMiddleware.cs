﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Filters;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.Primitives;


namespace Covid19.Middllewares
{
    internal class Token
    {
        [JsonProperty("APIKEY")]
        public Guid? Token1 { get; set; } = null;

        [JsonProperty("TOKEN")]
        public Guid? Token2 { set { Token1 = value; } }

        [JsonProperty("X-API-TOKEN")]
        public Guid? Token3 { set { Token1 = value; } }

        [JsonProperty("X-API-KEY")]
        public Guid? Token4 { set { Token1 = value; } }
        [JsonProperty("token")]
        public Guid? Token5 { set { Token1 = value; } }

        [JsonProperty("x-api-token")]
        public Guid? Token6 { set { Token1 = value; } }

        [JsonProperty("x-api-key")]
        public Guid? Token7 { set { Token1 = value; } }
        [JsonProperty("apikey")]
        public Guid? Token8 { set { Token1 = value; } }

    }
    public class TokenAuthorizationPipelineMiddleware
    {
        public Guid AccessToken { get; private set; }
        public Guid SessionToken { get; private set; }

        public Cookie Cookie { get; private set; }
        private StringValues extractedApiKey;
        private readonly RequestDelegate _next;
        private readonly string[] APIKEYNAME = { "APIKEY", "TOKEN", "X-API-TOKEN", "X-API-KEY" };
        private readonly string[] OPENENDPOINTS = { "/user/login", "/user/register", "/status" };
        public TokenAuthorizationPipelineMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context, TokenServices _tokenservice)
        {
            if (OPENENDPOINTS.Contains(context.Request.Path.ToString().ToLower()))
            {
                await _next(context);
            }
            else
            {
                bool foundKey = false;

                var req = context.Request;
                req.EnableBuffering();
                // Arguments: Stream, Encoding, detect encoding, buffer size 
                // AND, the most important: keep stream opened
                StreamReader reader = new StreamReader(req.Body);
                var bodyStr = reader.ReadToEndAsync();
                req.Body.Position = 0;
                // Rewind, so the core is not lost when it looks the body for the request
                Token body = null;
                try { body = JsonConvert.DeserializeObject<Token>(bodyStr.Result); } catch { }
                foundKey = (body is not null && body.Token1 is not null) ? true : false;
                if (foundKey)
                {
                    extractedApiKey = body.Token1.ToString();
                }
                foreach (var keyName in APIKEYNAME)
                {

                    if (!foundKey) foundKey = req.Query.TryGetValue(keyName, out extractedApiKey);
                    if (!foundKey) foundKey = req.Headers.TryGetValue(keyName, out extractedApiKey);
                    if (foundKey) break;
                }
                if (!foundKey)
                {

                }
                if (foundKey)
                {
                    _ = _tokenservice.VerifyAccess(extractedApiKey.ToString());
                    if (this.AccessToken == Guid.Empty)
                    {

                        return;
                    }

                    await _next(context);
                }

                if (!foundKey)
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("Api Key was not provided!");
                    return;
                }
            }
        }
    }
}

