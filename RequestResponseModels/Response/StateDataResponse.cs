﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestResponseModels.Response
{
    public class StateDataResponse
    {
        public string StateName { get; set; }
        public long TotalCases { get; set; }
        public long Recovered { get; set; }
        public long Active { get; set; }
        public long Decesed { get; set; }
    }
}
