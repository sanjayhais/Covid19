﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestResponseModels.Response
{
    public class StatusResponse
    {
        public string Version { get; set; }
        public string Status { get; set; }
        public string DatabaseName { get; set; }
    }
}
