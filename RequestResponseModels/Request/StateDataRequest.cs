﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RequestResponseModels.Request
{
    public class StateDataRequest
    {
        [Required]
        public string State { get; set; }
    }
}
