﻿using Covid19.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ITokenService
    {
        User CurrentUser { get; }
        Guid AccessToken { get; }
        (Guid, User) GetNewLoginToken(User currentUser);
        (Guid, User) VerifyAccess(string token);
    }
}
