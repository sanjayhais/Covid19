﻿using Covid19.DAL.CRUD;
using Covid19.DAL.Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
   
    public class TokenServices:ITokenService
    { 
        public User CurrentUser { get; private set; }
        public Guid AccessToken { get; private set; }

        public (Guid,User) GetNewLoginToken(User currentUser)
        {
            var crud = new TokenCRUD();
            (this.AccessToken, this.CurrentUser) = crud.NewToken(currentUser);
            return (this.AccessToken, this.CurrentUser);
        }

        public(Guid,User) VerifyAccess(string token)
        {
            var crud = new TokenCRUD();
            (this.AccessToken,this.CurrentUser)=crud.Verify(Guid.Parse(token));
            return (this.AccessToken, this.CurrentUser);
        }
    }
}
