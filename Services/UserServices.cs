﻿using Covid19.DAL.CRUD;
using Covid19.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UserServices
    {
        public User Register(string uEmail,string uName , string uPass)
        {
            var newUser = new User();
            newUser.Username = uName;
            newUser.Email = uEmail;
            newUser.Password = uPass;
            var crud = new UserCRUD();
            return crud.Register(newUser);

        }
        public User VerifyUser(string identifier , string passcode)
        {
            var crud = new UserCRUD();
            return crud.GetFirst(u => (u.Email == identifier || u.Username == identifier) && u.Password == passcode);
        }
    }
}
