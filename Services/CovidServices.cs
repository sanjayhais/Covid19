﻿using Covid19.DAL.CRUD;
using Covid19.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services 
{
    public class CovidServices
    {
        public Totaldatum StateData(string state )
        {
            var tCrud = new TotalCRUD();
            var sCrud = new StateCRUD();
            var stateId = sCrud.GetStateId(state);
            if(stateId == 0)
            {
                stateId = 1;
            }
            return tCrud.getStateData(stateId);
        }
    }
}
