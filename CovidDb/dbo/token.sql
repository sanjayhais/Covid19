﻿CREATE TABLE [dbo].[token]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[token] UNIQUEIDENTIFIER NOT NULL , 
    [userid] INT NOT NULL, 
    CONSTRAINT [FK_token_user] FOREIGN KEY ([userid]) REFERENCES [dbo].[user]([Id])
)
