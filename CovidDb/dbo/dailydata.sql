﻿CREATE TABLE [dbo].[dailydata]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [datetime] DATETIME2(0) NOT NULL,
    [stateid] INT NOT NULL,
    [newcases] INT NULL, 
    [recovered] INT NULL, 
    [deaths] INT NULL, 
    [active] INT NULL, 
    CONSTRAINT [FK_dailydata_states] FOREIGN KEY ([stateid]) REFERENCES [dbo].[states]([Id])
)
