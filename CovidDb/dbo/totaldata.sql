﻿CREATE TABLE [dbo].[totaldata]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [stateid] INT NOT NULL, 
    [totalcases] BIGINT NULL, 
    [totaldeaths] BIGINT NULL, 
    [totalrecovered] BIGINT NULL, 
    [totalactive] BIGINT NULL, 
    CONSTRAINT [FK_totaldata_states] FOREIGN KEY ([stateid]) REFERENCES [dbo].[states]([Id])
)
